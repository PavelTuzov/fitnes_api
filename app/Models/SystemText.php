<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemText extends Model
{
    use SoftDeletes;
    public $table = 'system_text';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'code',
        'description',
        'body',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
