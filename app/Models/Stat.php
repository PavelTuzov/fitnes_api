<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Stat extends Model
{
    public $table = 'statistics';

    protected $dates = [
        'created_at',
    ];

    protected $fillable = [
        'marathon_id',
        'step_id',
        'user_id',
        'updated_at',
        'created_at',
    ];
}
