<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Billing extends Model
{
    public $table = 'billing';

    protected $dates = [
        'created_at',
    ];

    protected $fillable = [
        'sum',
        'success',
        'user_id',
        'marathon_id',
        'step_id',
        'created_at',
    ];
}
