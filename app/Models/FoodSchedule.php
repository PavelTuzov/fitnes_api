<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class FoodSchedule extends Model
{
    use SoftDeletes;
    public $table = 'foodSchedule';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'published_at',
    ];

    protected $fillable = [
        'title',
        'description',
        'body',
        'created_at',
        'updated_at',
        'deleted_at',
        'published_at'
    ];
}
