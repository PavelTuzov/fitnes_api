<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Code extends Model
{
    use Notifiable;
    public $table = 'codes';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'confirm_at' => 'datetime',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
        'confirm_at',
    ];



    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
