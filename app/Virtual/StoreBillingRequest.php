<?php

/**
 * @OA\Schema(
 *      title="Store Billing request",
 *      description="Store Billing request body data",
 *      type="object",
 *      required={"sum"},
 *      required={"marathon_id"},
 *      required={"step_id"}
 * )
 */

class StoreBillingRequest
{
    /**
     * @OA\Property(
     *      title="sum",
     *      description="payment amount",
     *      example="100"
     * )
     *
     * @var string
     */
    public $sum;

    /**
     * @OA\Property(
     *      title="marathon_id",
     *      description="Marathon ID",
     *      example="1"
     * )
     *
     * @var int
     */
    public $marathon_id;

    /**
     * @OA\Property(
     *      title="step_id",
     *      description="Step ID",
     *      example="1"
     * )
     *
     * @var int
     */
    public $step_id;


}