<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="User token",
 *     description="User token",
 * )
 */
class Token
{
    /**
     * @OA\Property(
     *     title="access_token",
     *     description="access_token",
     *     format="string",
     *     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXBpL3YxL3VzZXJzL2xvZ2luIiwiaWF0IjoxNjI0OTY4OTU3LCJleHAiOjE2MjQ5NzI1NTcsIm5iZiI6MTYyNDk2ODk1NywianRpIjoiZWt5VXhlOVhmYnJWbUtueiIsInN1YiI6IjIiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.j1qvdHCVEGkIv3Ox1zsZk3dQQKQkeXlBC36jpvQrWSc"
     * )
     *
     * @var string
     */
    private $access_token;


    /**
     * @OA\Property(
     *     title="token_type",
     *     description="token type",
     *     format="string",
     *     example="bearer"
     * )
     *
     * @var string
     */
    private $token_type;

    /**
     * @OA\Property(
     *     title="expires_in",
     *     description="expires in",
     *     format="int64",
     *     example="3600"
     * )
     *
     * @var integer
     */
    private $expires_in;

}