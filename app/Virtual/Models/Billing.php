<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Billing",
 *     description="Billing model",
 * )
 */
class Billing
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;


    /**
     * @OA\Property(
     *     title="Marathon ID",
     *     description="Marathon ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $marathon_id;

    /**
     * @OA\Property(
     *     title="Marathon Step ID",
     *     description="Marathon Step ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $step_id;


    /**
     * @OA\Property(
     *     title="Created at",
     *     description="Created at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $created_at;

}