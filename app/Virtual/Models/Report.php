<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Report",
 *     description="Report model",
 * )
 */
class Report
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Marathon ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $marathon_id;

    /**
     * @OA\Property(
     *     title="Marathon Step ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $step_id;

    /**
     * @OA\Property(
     *     title="Calories lost",
     *     description="Calories lost",
     *     format="int64",
     *     example=100
     * )
     *
     * @var integer
     */
    private $calorie_lost;

    /**
     * @OA\Property(
     *     title="Weight",
     *     description="Weight",
     *     format="int64",
     *     example=50
     * )
     *
     * @var integer
     */
    private $weight;

    /**
     * @OA\Property(
     *     title="Spent of time",
     *     description="Spent of time current step",
     *     format="int64",
     *     example=15
     * )
     *
     * @var integer
     */
    private $time;

    /**
     * @OA\Property(
     *     title="Photo",
     *     description="Proof of",
     *     format="binary"
     * )
     *
     * @var string
     */
    private $photo;


    /**
     * @OA\Property(
     *     title="Created at",
     *     description="Created at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $created_at;

}