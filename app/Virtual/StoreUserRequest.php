<?php

/**
 * @OA\Schema(
 *      title="Store User request",
 *      description="Store User request body data",
 *      type="object",
 *      required={"name"}
 * )
 */

class StoreUserRequest
{
    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the new user",
     *      example="Pavel"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="surname",
     *      description="Surname of the new user",
     *      example="Tuzov"
     * )
     *
     * @var string
     */
    public $surname;

    /**
     * @OA\Property(
     *      title="phone",
     *      description="Phone of the new user",
     *      example="79126926537"
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *      title="email",
     *      description="Email of the new user",
     *      example="mail@ptuzov.ru"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="password",
     *      description="Password of the new user",
     *      example="123456"
     * )
     *
     * @var string
     */
    public $password;


}