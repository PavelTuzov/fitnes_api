<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="SystemTextResource",
 *     description="SystemText resource",
 *     @OA\Xml(
 *         name="SystemTextResource"
 *     )
 * )
 */
class SystemTextResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\SystemText[]
     */
    private $data;
}
