<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="ProgramResource",
 *     description="Program resource",
 *     @OA\Xml(
 *         name="ProgramResource"
 *     )
 * )
 */
class ProgramResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\Program[]
     */
    private $data;
}
