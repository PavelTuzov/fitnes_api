<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="FoodScheduleResource",
 *     description="Food Schedule resource",
 *     @OA\Xml(
 *         name="FoodScheduleResource"
 *     )
 * )
 */
class FoodScheduleResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\FoodSchedule[]
     */
    private $data;
}
