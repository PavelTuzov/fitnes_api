<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="MarathonResource",
 *     description="Marathon resource",
 *     @OA\Xml(
 *         name="MarathonResource"
 *     )
 * )
 */
class MarathonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\Marathon[]
     */
    private $data;
}
