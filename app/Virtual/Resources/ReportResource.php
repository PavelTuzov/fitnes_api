<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="ReportResource",
 *     description="Report resource",
 *     @OA\Xml(
 *         name="ReportResource"
 *     )
 * )
 */
class ReportResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\Report[]
     */
    private $data;
}
