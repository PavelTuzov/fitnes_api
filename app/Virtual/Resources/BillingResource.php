<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="BillingResource",
 *     description="Billing resource",
 *     @OA\Xml(
 *         name="BillingResource"
 *     )
 * )
 */
class BillingResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\Billing[]
     */
    private $data;
}
