<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="TrainingResource",
 *     description="Training resource",
 *     @OA\Xml(
 *         name="TrainingResource"
 *     )
 * )
 */
class TrainingResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\Training[]
     */
    private $data;
}
