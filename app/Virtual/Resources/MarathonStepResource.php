<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="MarathonStepResource",
 *     description="MarathonStep resource",
 *     @OA\Xml(
 *         name="MarathonStepResource"
 *     )
 * )
 */
class MarathonStepResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\MarathonStep[]
     */
    private $data;
}
