<?php

/**
 * @OA\Schema(
 *      title="Login User request",
 *      description="Login User request body data",
 *      type="object",
 *      required={"email"}
 * )
 */

class LoginUserRequest
{

    /**
     * @OA\Property(
     *      title="email",
     *      description="Email of the new user",
     *      example="mail@ptuzov.ru"
     * )
     *
     * @var string
     */
    public $email;


    /**
     * @OA\Property(
     *      title="password",
     *      description="Password of the new user",
     *      example="123456"
     * )
     *
     * @var string
     */
    public $password;
}