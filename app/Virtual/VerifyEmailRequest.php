<?php

/**
 * @OA\Schema(
 *      title="Send verify emai request",
 *      description="Send verify code to user email",
 *      type="object",
 *      required={"email"}
 * )
 */

class VerifyEmailRequest
{

    /**
     * @OA\Property(
     *      title="email",
     *      description="Email of the new user",
     *      example="mail@ptuzov.ru"
     * )
     *
     * @var string
     */
    public $email;
}