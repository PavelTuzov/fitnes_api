<?php

/**
 * @OA\Schema(
 *      title="Store Program request",
 *      description="Store Program request body data",
 *      type="object",
 *      required={"title"}
 * )
 */

class StoreProgramRequest
{
    /**
     * @OA\Property(
     *      title="title",
     *      description="Title of the new program",
     *      example="example title program"
     * )
     *
     * @var string
     */
    public $title;

    /**
     * @OA\Property(
     *      title="body",
     *      description="Body of the new program",
     *      example="example body program"
     * )
     *
     * @var string
     */
    public $body;

}