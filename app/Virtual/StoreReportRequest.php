<?php

/**
 * @OA\Schema(
 *      title="Store Report request",
 *      description="Store Report request body data",
 *      type="object",
 *      required={"time"},
 *      required={"calories"},
 *      required={"weight"},
 *      required={"marathon_id"},
 *      required={"step_id"}
 * )
 */

class StoreReportRequest
{
    /**
     * @OA\Property(
     *      title="time",
     *      description="spent time",
     *      example="15"
     * )
     *
     * @var int
     */
    public $time;

    /**
     * @OA\Property(
     *      title="weight",
     *      description="current weight",
     *      example="60"
     * )
     *
     * @var int
     */
    public $weight;

    /**
     * @OA\Property(
     *      title="Calories lost",
     *      description="current calories lost",
     *      example="200"
     * )
     *
     * @var int
     */
    public $calorie_lost;

    /**
     * @OA\Property(
     *      title="marathon_id",
     *      description="Marathon ID",
     *      example="1"
     * )
     *
     * @var int
     */
    public $marathon_id;

    /**
     * @OA\Property(
     *      title="step_id",
     *      description="Step ID",
     *      example="1"
     * )
     *
     * @var int
     */
    public $step_id;


}