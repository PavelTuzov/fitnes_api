<?php

/**
 * @OA\Schema(
 *      title="Update Project request",
 *      description="Update Project request body data",
 *      type="object",
 *      required={"name"}
 * )
 */

class UpdateUserRequest
{
    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the new user",
     *      example="A nice user"
     * )
     *
     * @var string
     */
    public $name;

}