<?php

/**
 * @OA\Schema(
 *      title="Send verify phone request",
 *      description="Send verify code to user phone",
 *      type="object",
 *      required={"phone"}
 * )
 */

class VerifyPhoneRequest
{

    /**
     * @OA\Property(
     *      title="phone",
     *      description="Phone of the new user",
     *      example="79126926537"
     * )
     *
     * @var string
     */
    public $phone;
}