<?php

namespace App\Services;

use App\Http\Requests\User\LoginUserRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Mail\MailVerify;
use App\Models\Code;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class UserService extends Service
{

    /**
     * Store new user
     *
     * @param StoreUserRequest $request
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        return $user;
    }

    /**
     * Store new user
     *
     * @param LoginUserRequest $request
     * @return mixed
     */
    public function login(LoginUserRequest $request)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $token;
    }

    public function logout()
    {
        auth()->logout();
    }

    /**
     * @throws \Exception
     */
    public function sendVerifyEmail($email, $user)
    {
        $code = $this->makeCode('email', $user);
        Mail::to($email)->send(new MailVerify($email, $code));


        return true;
    }


    public function sendVerifyPhone($get, $user)
    {
        $code = $this->makeCode('phone', $user);

        return true;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserData()
    {
        return auth()->user();
    }

    private function makeCode(string $type, $user)
    {
        Code::where('type', $type)->where('user_id', $user->id)->whereNull('confirm_at')->delete();
        $code = new Code();
        $code->type = $type;
        $code->code = CodeService::generate();
        $code->user_id = $user->id;
        $code->save();

        return $code;
    }


}