<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //

        /*Gate::define('update-post', function (User $user, Post $post) {
            return $user->id === $post->user_id;
        });*/

        Gate::define('users_access', function (User $user) {
            return $user->isAdmin;
        });

        Gate::define('user_show', function (User $user, User $showUser) {
            return $user->isAdmin || $user->id === $showUser->id;
        });

        Gate::define('user_edit', function (User $user, User $editUser) {
            return $user->isAdmin || $user->id === $editUser->id;
        });

        Gate::define('user_delete', function (User $user) {
            return $user->isAdmin;
        });
    }
}
