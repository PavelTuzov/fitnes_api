<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\ProgramResource;
use App\Http\Resources\UserResource;
use App\Models\Marathon;
use App\Models\MarathonStep;
use App\Models\Program;
use App\Models\User;
use App\Virtual\Resources\MarathonResource;
use App\Virtual\Resources\MarathonStepResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Gate;
use StoreProgramRequest;
use Symfony\Component\HttpFoundation\Response;


class MarathonController extends Controller
{
    /**
     * @OA\Get(
     *      path="/marathons",
     *      operationId="getMarathonList",
     *      tags={"Marathons"},
     *      summary="Get list of marathons",
     *      description="Returns list of marathons",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MarathonResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        abort_if(Gate::denies('marathon_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MarathonResource(Marathon::with(['steps'])->get());
    }

    /**
     * @OA\Get(
     *      path="/marathons/{id}",
     *      operationId="getMarathonById",
     *      tags={"Marathons"},
     *      summary="Get marathon information",
     *      description="Returns marathon data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Marathon id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Marathon")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(Marathon $marathon)
    {
        abort_if(Gate::denies('marathon_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MarathonResource($marathon);
    }

    /**
     * @OA\Get(
     *      path="/marathons/steps/{id}",
     *      operationId="getMarathonStepsByMarathonId",
     *      tags={"Marathons"},
     *      summary="Get marathon steps information",
     *      description="Returns marathon steps data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Marathon id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Marathon")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function showStepsByMarathon(Marathon $marathon)
    {
        abort_if(Gate::denies('marathon_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MarathonResource($marathon);
    }


    /**
     * @OA\Get(
     *      path="/marathons/step/{id}",
     *      operationId="getMarathonStepById",
     *      tags={"Marathons"},
     *      summary="Get marathon step information",
     *      description="Returns marathon step data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Marathon step id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MarathonStep")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function showMarathonStep(MarathonStep $marathonStep)
    {
        abort_if(Gate::denies('step_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MarathonStepResource($marathonStep);
    }
}