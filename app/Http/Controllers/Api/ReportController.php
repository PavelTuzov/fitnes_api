<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\Billing;
use App\Models\Report;
use App\Models\User;
use App\Virtual\Resources\BillingResource;
use App\Virtual\Resources\ReportResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use StoreBillingRequest;
use StoreReportRequest;
use Symfony\Component\HttpFoundation\Response;


class ReportController extends Controller
{


    /**
     * @OA\Post(
     *      path="/report",
     *      operationId="storeReport",
     *      tags={"Report"},
     *      summary="Store new report",
     *      description="Returns report data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreReportRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Report")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreReportRequest $request)
    {
        $user = Auth::user();
        $report = Report::create($request->all);
        $report->user_id = $user->id;

        return (new ReportResource($report))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
}