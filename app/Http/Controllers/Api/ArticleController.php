<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FoodScheduleResource;
use App\Models\Article;
use App\Models\FoodSchedule;
use App\Models\Training;
use App\Virtual\Resources\ArticleResource;
use App\Virtual\Resources\TrainingResource;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;


class ArticleController extends Controller
{
    /**
     * @OA\Get(
     *      path="/article",
     *      operationId="getArticleList",
     *      tags={"Article"},
     *      summary="Get list of articles",
     *      description="Returns list of articles",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ArticleResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        //abort_if(Gate::denies('article_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArticleResource(Article::get());
    }


    /**
     * @OA\Get(
     *      path="/article/{id}",
     *      operationId="getArticleById",
     *      tags={"Article"},
     *      summary="Get article information",
     *      description="Returns article data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Article id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Article")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(Article $article)
    {
        //abort_if(Gate::denies('food_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArticleResource($article);
    }
}