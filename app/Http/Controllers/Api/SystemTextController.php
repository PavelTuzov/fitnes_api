<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommonResource;
use App\Models\SystemText;
use App\Virtual\Resources\SystemTextResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class SystemTextController extends Controller
{
    /**
     * @OA\Get(
     *      path="/system-text/legal",
     *      operationId="getLegalText",
     *      tags={"SystemText"},
     *      summary="Get text of legal",
     *      description="Returns text of legal",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/SystemTextResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function getSystemText(Request $request, $code = null)
    {
        abort_if(is_null($code), Response::HTTP_FORBIDDEN, '403 Forbidden');
        //abort_if(Gate::denies('article_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CommonResource(SystemText::where('code', $code)->first());
    }


}