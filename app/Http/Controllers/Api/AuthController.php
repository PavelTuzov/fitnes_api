<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginUserRequest;
use App\Http\Requests\User\VerifyEmailRequest;
use App\Http\Requests\User\VerifyPhoneRequest;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;


class AuthController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * Конструктор
     *
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * @OA\Post(
     *      path="/users/login",
     *      operationId="loginUser",
     *      tags={"Users"},
     *      summary="Login user",
     *      description="Returns user tokens",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/LoginUserRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Token")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function login(LoginUserRequest $request): \Illuminate\Http\JsonResponse
    {
        $data = $this->userService->login($request);
        return $this->respondWithToken($data);
    }

    /**
     * @throws \Exception
     *
     * @OA\Post(
     *      path="/users/sendVerifyEmail",
     *      operationId="sendVerifyEmail",
     *      tags={"Users"},
     *      summary="Send verify email",
     *      description="Returns bool",
     *      security={{"bearerAuth":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/VerifyEmailRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function sendVerifyEmail(VerifyEmailRequest $request)
    {
        abort_if(Gate::denies('users_access', [Auth::user()]), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $data = $this->userService->sendVerifyEmail($request->get('email'), Auth::user());

        return response()->json([], Response::HTTP_ACCEPTED);

    }

    /**
     * @throws \Exception
     *
     * @OA\Post(
     *      path="/users/sendVerifyPhone",
     *      operationId="sendVerifyPhone",
     *      tags={"Users"},
     *      summary="Send verify phone",
     *      description="Returns bool",
     *      security={{"bearerAuth":{}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/VerifyPhoneRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function sendVerifyPhone(VerifyPhoneRequest $request)
    {
        abort_if(Gate::denies('users_access', [Auth::user()]), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $data = $this->userService->sendVerifyPhone($request->get('phone'), Auth::user());

        return response($data, Response::HTTP_ACCEPTED);
    }

    /**
     * Get the authenticated User.
     *
     * @return UserResource
     *
     * @OA\Post(
     *      path="/users/me",
     *      operationId="me",
     *      tags={"Users"},
     *      summary="Returns user information",
     *      description="Returns User data",
     *      security={{"bearerAuth":{}}},
     *
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function me()
    {
        abort_if(Gate::denies('users_access', [Auth::user()]), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserResource($this->userService->getUserData());
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * * @OA\Post(
     *      path="/users/refresh",
     *      operationId="refreshToken",
     *      tags={"Users"},
     *      summary="Refresh user token",
     *      description="Returns user tokens",
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Token")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }


    /**
     * @OA\Post(
     *      path="/users/logout",
     *      operationId="logoutUser",
     *      tags={"Users"},
     *      summary="Logout user",
     *      description="Unlogin user session",
     *      security={{"bearerAuth":{}}},
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function logout()
    {
        $this->userService->logout();

        return response(null, Response::HTTP_NO_CONTENT);
    }

}