<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\Billing;
use App\Models\User;
use App\Virtual\Resources\BillingResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use StoreBillingRequest;
use Symfony\Component\HttpFoundation\Response;


class BillingController extends Controller
{
    /**
     * @OA\Get(
     *      path="/billing",
     *      operationId="getPaymentHistory",
     *      tags={"Billing"},
     *      summary="Get list of payments",
     *      description="Returns list of payments",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BillingResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        abort_if(Gate::denies('billing_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BillingResource(Billing::get());
    }

    /**
     * @OA\Post(
     *      path="/billing",
     *      operationId="storePayment",
     *      tags={"Billing"},
     *      summary="Store new payment",
     *      description="Returns payment data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreBillingRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Billing")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreBillingRequest $request)
    {
        $user = Auth::user();
        $billing = Billing::create($request->all);
        $billing->user_id = $user->id;

        return (new BillingResource($billing))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
}