<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Training;
use App\Virtual\Resources\TrainingResource;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;


class TrainingController extends Controller
{
    /**
     * @OA\Get(
     *      path="/trainings",
     *      operationId="getTrainingsList",
     *      tags={"Trainings"},
     *      summary="Get list of trainings",
     *      description="Returns list of trainings",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TrainingResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        abort_if(Gate::denies('training_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrainingResource(Training::get());
    }

    /**
     * @OA\Get(
     *      path="/trainings/byProgram/{id}",
     *      operationId="getTrainingsListByProgram",
     *      tags={"Trainings"},
     *      summary="Get list of trainings by program",
     *      description="Returns list of trainings by program",
     *     @OA\Parameter(
     *          name="program_id",
     *          description="Program id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TrainingResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function byProgram(int $id)
    {
        abort_if(Gate::denies('training_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrainingResource(Training::where('program_id', $id)->get());
    }


    /**
     * @OA\Get(
     *      path="/trainings/{id}",
     *      operationId="getTrainingById",
     *      tags={"Trainings"},
     *      summary="Get training information",
     *      description="Returns training data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Training id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Training")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(Training $training)
    {
        abort_if(Gate::denies('training_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrainingResource($training);
    }
}