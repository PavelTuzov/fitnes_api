<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FoodScheduleResource;
use App\Models\FoodSchedule;
use App\Models\Training;
use App\Virtual\Resources\TrainingResource;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;


class FoodScheduleController extends Controller
{
    /**
     * @OA\Get(
     *      path="/food",
     *      operationId="getFoodScheduleList",
     *      tags={"FoodSchedule"},
     *      summary="Get list of food schedule",
     *      description="Returns list of food schedule",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/FoodScheduleResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        abort_if(Gate::denies('food_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FoodScheduleResource(FoodSchedule::get());
    }


    /**
     * @OA\Get(
     *      path="/food/{id}",
     *      operationId="getFoodScheduleById",
     *      tags={"FoodSchedule"},
     *      summary="Get food schedule information",
     *      description="Returns food schedule data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Food schedule id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/FoodSchedule")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(FoodSchedule $foodSchedule)
    {
        abort_if(Gate::denies('food_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FoodScheduleResource($foodSchedule);
    }
}