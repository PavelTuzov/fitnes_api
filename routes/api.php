<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => 'v1',
    'as' => 'api',
    'namespace' => 'App\Http\Controllers\Api',
    'middleware' => ['api']
], function () {
    //Route::apiResource('projects', 'ProjectController');
    Route::group(['middleware' => ['auth']], function() {
        Route::apiResource('users', 'UserController');
        Route::post('/users/logout', 'AuthController@logout')->name('auth.logout');
        Route::post('/users/me', 'AuthController@me')->name('auth.me');
        Route::post('/users/sendVerifyEmail', 'AuthController@sendVerifyEmail')->name('auth.sendVerifyEmail');
    });
    Route::post('/users/login', 'AuthController@login')->name('auth.login');
    Route::post('/users/refresh', 'AuthController@refresh')->name('auth.refresh');


    Route::get('/system-text/{code}', 'SystemTextController@getSystemText')->name('sytemText.get');



});

